package com.aps

import io.gatling.core.Predef._ // 2
import io.gatling.http.Predef._
import scala.concurrent.duration._
import Utilities.Constants
import Utilities.BaseHelpers

class CreateAccount extends Simulation{
 val httpConfig=http.baseURL(Constants.BASEURL)
 val headerVal=Map("Authorization"->Constants.APIKEY,"Content-Type"->"application/json")

  val scn=scenario("AccountCreationSimulation")
    .exec(session=>session.set("randomString",BaseHelpers.RandomStringGenerator(5)))
    .exec(http("AccountRequest")
    .post("/accounts?environment="+Constants.ENVIRONMENT+"&organizationId="+Constants.ORGANIZATIONID+"&customerId="+Constants.CUSTOMERID)
    .headers(headerVal)
      .body(ElFileBody("src/test/resources/data/account.json")).asJSON
    //.body(ElFileBody("src/test/resources/data/account.json")).asJSON
      .check(status.is(201), bodyString.saveAs("Response")))
      .exec{session =>
        val r = session.get("Response")
         println("Response=> " + r)
         session}
    .pause(5)

  setUp( // 11
    scn.inject(
      atOnceUsers(Constants.USERSATONCE))
      //rampUsers(Constants.USERSATONCE) over (5 seconds))
      //constantUsersPerSec(Constants.USERSATONCE) during (15 seconds))
  ).protocols(httpConfig)

before(
  println("Simulation just started !!")
)
after("Simulation just ended !!")
}


