package com.aps

import io.gatling.core.Predef._ // 2
import io.gatling.http.Predef._
import scala.concurrent.duration._

class CreateOrg extends Simulation{

  def randomGenerator():String={
    val random=scala.util.Random.alphanumeric.take(15).mkString
    return random
  }

  val httpConfig=http.baseURL("http://discovery-alpha.core.cvent.org/account-provisioning-service-alpha/accountprovisioning/v1")
  val headerVal=Map("Authorization"->"api_key 995723ac147b6f681dae28963541f314","Content-Type"->"application/json")
  val scn=scenario("create Organization")
    .exec(session=>session.set("randomNumberForOrg",randomGenerator()))
    .exec(http("createOrganization")
      .post("/organizations?environment=S401")
      .headers(headerVal)
      .body(ElFileBody("src/test/resources/data/organization.json")).asJSON
      //.body(ElFileBody("src/test/resources/data/account.json")).asJSON
      .check(status.is(201)))
    .pause(5)

  setUp( // 11
    scn.inject(
      atOnceUsers(10),
      rampUsers(20) over(2 seconds))
  ).protocols(httpConfig)
}
