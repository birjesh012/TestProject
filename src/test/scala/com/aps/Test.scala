package com.aps

class Test extends Simulation {
    val params = new Constants();
    val httpConfig = http.baseURLs(params.host)
    val requests = new util.ArrayList[String]()


    def randomGenerator() : Int = {
      val randomNum = new Random().nextInt(100000)
      return randomNum
    }

    val headerVal = Map("Authorization"->params.api_key,
      "Content-Type"->"application/json")

    val scnRequest = scenario("createRequest")
      .exec(session=>session.set("randomNumber",randomGenerator()))
      .exec(http("createAccountRequest")
        .post(params.accountRequestUrl)
        .headers(headerVal)
        .body(ElFileBody(".//config//AccountRequestNew.json")).asJSON
        .check(status.is(201))
        .check(jsonPath("$.id").saveAs("Response_Data")))
      .pause(10)



      .exec( session => {
        //println(session("Response_Data").as[String])
        requests.add(session("Response_Data").as[String])
        session

      })


    val scnActivate = scenario("activateAccount")
      .exec(http("activateAccountRequest")
        .post("/activate/" +requests.foreach{ i => (i)}+"?environment=S432")
        .headers(headerVal)
        .check(status.is(201)))

      .exec( session => {
        println("/activate/" +requests.foreach{ i => (i)}+"?environment=S432")
        session
      })


    setUp(scnRequest.inject(constantUsersPerSec(10) during(1)),
      scnActivate.inject(nothingFor(5), constantUsersPerSec(6) during(1))
    ).protocols(httpConfig)
    before(
      println("Simulation just started !!")
    )
    after(
      //println()
      requests.foreach{ i => println(i)}


    )
  }

}
