package Utilities

object BaseHelpers {

  def RandomChar():Char={
    val low = 65 // A
    val high = 90 // Z
    return (util.Random.nextInt(high - low) + low).toChar
  }

  //Code to Generate a random String (a-z)
  def RandomStringGenerator(length: Int):String = {
    val sb = new StringBuilder
    for (i <- 1 to length) {
      val r=RandomChar()
      sb.append(r)
    }
    return sb.toString
  }
  // code to generate random number of six digit
  def RandomNumberGenerator():Int={
    val random = util.Random.nextInt(900000)+100000
    return random
  }

  // code to generate random alphanumeric string
  def RandomAlphanumericGenerator(length: Int): String={
    val r = new scala.util.Random
    val sb = new StringBuilder
    for (i <- 1 to length) {
      sb.append(r.nextPrintableChar)
    }
    return sb.toString

  }

}
