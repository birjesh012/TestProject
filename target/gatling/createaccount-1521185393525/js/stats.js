var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2121",
        "ok": "2121",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10049",
        "ok": "10049",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6230",
        "ok": "6230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2535",
        "ok": "2535",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6005",
        "ok": "6005",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8469",
        "ok": "8469",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9951",
        "ok": "9951",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10043",
        "ok": "10043",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 100,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "6.25",
        "ok": "6.25",
        "ko": "-"
    }
},
contents: {
"req_accountrequest-37419": {
        type: "REQUEST",
        name: "AccountRequest",
path: "AccountRequest",
pathFormatted: "req_accountrequest-37419",
stats: {
    "name": "AccountRequest",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2121",
        "ok": "2121",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10049",
        "ok": "10049",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6230",
        "ok": "6230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2535",
        "ok": "2535",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6005",
        "ok": "6005",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8469",
        "ok": "8469",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9951",
        "ok": "9951",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10043",
        "ok": "10043",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 100,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "6.25",
        "ok": "6.25",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
