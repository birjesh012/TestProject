var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "250",
        "ok": "217",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "1888",
        "ok": "1888",
        "ko": "4492"
    },
    "maxResponseTime": {
        "total": "32555",
        "ok": "32555",
        "ko": "16996"
    },
    "meanResponseTime": {
        "total": "20986",
        "ok": "21937",
        "ko": "14737"
    },
    "standardDeviation": {
        "total": "8149",
        "ok": "8296",
        "ko": "2346"
    },
    "percentiles1": {
        "total": "21427",
        "ok": "23577",
        "ko": "15180"
    },
    "percentiles2": {
        "total": "27480",
        "ok": "28634",
        "ko": "16086"
    },
    "percentiles3": {
        "total": "31837",
        "ok": "31914",
        "ko": "16841"
    },
    "percentiles4": {
        "total": "32497",
        "ok": "32515",
        "ko": "16976"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 217,
        "percentage": 87
    },
    "group4": {
        "name": "failed",
        "count": 33,
        "percentage": 13
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "6.579",
        "ok": "5.711",
        "ko": "0.868"
    }
},
contents: {
"req_accountrequest-37419": {
        type: "REQUEST",
        name: "AccountRequest",
path: "AccountRequest",
pathFormatted: "req_accountrequest-37419",
stats: {
    "name": "AccountRequest",
    "numberOfRequests": {
        "total": "250",
        "ok": "217",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "1888",
        "ok": "1888",
        "ko": "4492"
    },
    "maxResponseTime": {
        "total": "32555",
        "ok": "32555",
        "ko": "16996"
    },
    "meanResponseTime": {
        "total": "20986",
        "ok": "21937",
        "ko": "14737"
    },
    "standardDeviation": {
        "total": "8149",
        "ok": "8296",
        "ko": "2346"
    },
    "percentiles1": {
        "total": "21427",
        "ok": "23577",
        "ko": "15180"
    },
    "percentiles2": {
        "total": "27480",
        "ok": "28634",
        "ko": "16086"
    },
    "percentiles3": {
        "total": "31837",
        "ok": "31914",
        "ko": "16841"
    },
    "percentiles4": {
        "total": "32497",
        "ok": "32515",
        "ko": "16976"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 217,
        "percentage": 87
    },
    "group4": {
        "name": "failed",
        "count": 33,
        "percentage": 13
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "6.579",
        "ok": "5.711",
        "ko": "0.868"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
