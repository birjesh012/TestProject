var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "2",
        "ok": "0",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "609",
        "ok": "-",
        "ko": "609"
    },
    "maxResponseTime": {
        "total": "1347",
        "ok": "-",
        "ko": "1347"
    },
    "meanResponseTime": {
        "total": "978",
        "ok": "-",
        "ko": "978"
    },
    "standardDeviation": {
        "total": "369",
        "ok": "-",
        "ko": "369"
    },
    "percentiles1": {
        "total": "978",
        "ok": "-",
        "ko": "978"
    },
    "percentiles2": {
        "total": "1163",
        "ok": "-",
        "ko": "1163"
    },
    "percentiles3": {
        "total": "1310",
        "ok": "-",
        "ko": "1310"
    },
    "percentiles4": {
        "total": "1340",
        "ok": "-",
        "ko": "1340"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 2,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.125",
        "ok": "-",
        "ko": "0.125"
    }
},
contents: {
"req_createorganizat-aeddb": {
        type: "REQUEST",
        name: "createOrganization",
path: "createOrganization",
pathFormatted: "req_createorganizat-aeddb",
stats: {
    "name": "createOrganization",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "1347",
        "ok": "-",
        "ko": "1347"
    },
    "maxResponseTime": {
        "total": "1347",
        "ok": "-",
        "ko": "1347"
    },
    "meanResponseTime": {
        "total": "1347",
        "ok": "-",
        "ko": "1347"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "1347",
        "ok": "-",
        "ko": "1347"
    },
    "percentiles2": {
        "total": "1347",
        "ok": "-",
        "ko": "1347"
    },
    "percentiles3": {
        "total": "1347",
        "ok": "-",
        "ko": "1347"
    },
    "percentiles4": {
        "total": "1347",
        "ok": "-",
        "ko": "1347"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.062",
        "ok": "-",
        "ko": "0.062"
    }
}
    },"req_accountrequest-37419": {
        type: "REQUEST",
        name: "AccountRequest",
path: "AccountRequest",
pathFormatted: "req_accountrequest-37419",
stats: {
    "name": "AccountRequest",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "609",
        "ok": "-",
        "ko": "609"
    },
    "maxResponseTime": {
        "total": "609",
        "ok": "-",
        "ko": "609"
    },
    "meanResponseTime": {
        "total": "609",
        "ok": "-",
        "ko": "609"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "609",
        "ok": "-",
        "ko": "609"
    },
    "percentiles2": {
        "total": "609",
        "ok": "-",
        "ko": "609"
    },
    "percentiles3": {
        "total": "609",
        "ok": "-",
        "ko": "609"
    },
    "percentiles4": {
        "total": "609",
        "ok": "-",
        "ko": "609"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.062",
        "ok": "-",
        "ko": "0.062"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
