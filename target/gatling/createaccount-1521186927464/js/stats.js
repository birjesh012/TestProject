var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "250",
        "ok": "249",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "1819",
        "ok": "1819",
        "ko": "17340"
    },
    "maxResponseTime": {
        "total": "23760",
        "ok": "23760",
        "ko": "17340"
    },
    "meanResponseTime": {
        "total": "13538",
        "ok": "13523",
        "ko": "17340"
    },
    "standardDeviation": {
        "total": "6584",
        "ok": "6593",
        "ko": "0"
    },
    "percentiles1": {
        "total": "14202",
        "ok": "14165",
        "ko": "17340"
    },
    "percentiles2": {
        "total": "19455",
        "ok": "19469",
        "ko": "17340"
    },
    "percentiles3": {
        "total": "23477",
        "ok": "23477",
        "ko": "17340"
    },
    "percentiles4": {
        "total": "23712",
        "ok": "23712",
        "ko": "17340"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 249,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "8.621",
        "ok": "8.586",
        "ko": "0.034"
    }
},
contents: {
"req_accountrequest-37419": {
        type: "REQUEST",
        name: "AccountRequest",
path: "AccountRequest",
pathFormatted: "req_accountrequest-37419",
stats: {
    "name": "AccountRequest",
    "numberOfRequests": {
        "total": "250",
        "ok": "249",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "1819",
        "ok": "1819",
        "ko": "17340"
    },
    "maxResponseTime": {
        "total": "23760",
        "ok": "23760",
        "ko": "17340"
    },
    "meanResponseTime": {
        "total": "13538",
        "ok": "13523",
        "ko": "17340"
    },
    "standardDeviation": {
        "total": "6584",
        "ok": "6593",
        "ko": "0"
    },
    "percentiles1": {
        "total": "14202",
        "ok": "14165",
        "ko": "17340"
    },
    "percentiles2": {
        "total": "19455",
        "ok": "19469",
        "ko": "17340"
    },
    "percentiles3": {
        "total": "23477",
        "ok": "23477",
        "ko": "17340"
    },
    "percentiles4": {
        "total": "23712",
        "ok": "23712",
        "ko": "17340"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 249,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "8.621",
        "ok": "8.586",
        "ko": "0.034"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
