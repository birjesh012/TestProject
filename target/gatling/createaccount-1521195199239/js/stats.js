var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1500",
        "ok": "794",
        "ko": "706"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "2246",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "59968",
        "ok": "59968",
        "ko": "18117"
    },
    "meanResponseTime": {
        "total": "17636",
        "ok": "30905",
        "ko": "2713"
    },
    "standardDeviation": {
        "total": "19080",
        "ok": "16857",
        "ko": "5759"
    },
    "percentiles1": {
        "total": "12394",
        "ok": "31438",
        "ko": "0"
    },
    "percentiles2": {
        "total": "32533",
        "ok": "45355",
        "ko": "0"
    },
    "percentiles3": {
        "total": "54699",
        "ok": "57438",
        "ko": "16450"
    },
    "percentiles4": {
        "total": "59519",
        "ok": "59706",
        "ko": "17671"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 794,
        "percentage": 53
    },
    "group4": {
        "name": "failed",
        "count": 706,
        "percentage": 47
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "19.737",
        "ok": "10.447",
        "ko": "9.289"
    }
},
contents: {
"req_accountrequest-37419": {
        type: "REQUEST",
        name: "AccountRequest",
path: "AccountRequest",
pathFormatted: "req_accountrequest-37419",
stats: {
    "name": "AccountRequest",
    "numberOfRequests": {
        "total": "1500",
        "ok": "794",
        "ko": "706"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "2246",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "59968",
        "ok": "59968",
        "ko": "18117"
    },
    "meanResponseTime": {
        "total": "17636",
        "ok": "30905",
        "ko": "2713"
    },
    "standardDeviation": {
        "total": "19080",
        "ok": "16857",
        "ko": "5759"
    },
    "percentiles1": {
        "total": "12394",
        "ok": "31438",
        "ko": "0"
    },
    "percentiles2": {
        "total": "32533",
        "ok": "45355",
        "ko": "0"
    },
    "percentiles3": {
        "total": "54699",
        "ok": "57438",
        "ko": "16450"
    },
    "percentiles4": {
        "total": "59519",
        "ok": "59706",
        "ko": "17671"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 794,
        "percentage": 53
    },
    "group4": {
        "name": "failed",
        "count": 706,
        "percentage": 47
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "19.737",
        "ok": "10.447",
        "ko": "9.289"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
