var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "200",
        "ok": "200",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3502",
        "ok": "3502",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20995",
        "ok": "20995",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12274",
        "ok": "12274",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5739",
        "ok": "5739",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11567",
        "ok": "11567",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17523",
        "ok": "17523",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20809",
        "ok": "20809",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20986",
        "ok": "20986",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 200,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "7.407",
        "ok": "7.407",
        "ko": "-"
    }
},
contents: {
"req_createuser-2810c": {
        type: "REQUEST",
        name: "createUser",
path: "createUser",
pathFormatted: "req_createuser-2810c",
stats: {
    "name": "createUser",
    "numberOfRequests": {
        "total": "200",
        "ok": "200",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3502",
        "ok": "3502",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20995",
        "ok": "20995",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12274",
        "ok": "12274",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5739",
        "ok": "5739",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11567",
        "ok": "11567",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17523",
        "ok": "17523",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20809",
        "ok": "20809",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20986",
        "ok": "20986",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 200,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "7.407",
        "ok": "7.407",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
