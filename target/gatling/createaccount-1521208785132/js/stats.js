var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "2",
        "ok": "0",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "561",
        "ok": "-",
        "ko": "561"
    },
    "maxResponseTime": {
        "total": "674",
        "ok": "-",
        "ko": "674"
    },
    "meanResponseTime": {
        "total": "618",
        "ok": "-",
        "ko": "618"
    },
    "standardDeviation": {
        "total": "57",
        "ok": "-",
        "ko": "57"
    },
    "percentiles1": {
        "total": "618",
        "ok": "-",
        "ko": "618"
    },
    "percentiles2": {
        "total": "646",
        "ok": "-",
        "ko": "646"
    },
    "percentiles3": {
        "total": "668",
        "ok": "-",
        "ko": "668"
    },
    "percentiles4": {
        "total": "673",
        "ok": "-",
        "ko": "673"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 2,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.125",
        "ok": "-",
        "ko": "0.125"
    }
},
contents: {
"req_createorganizat-aeddb": {
        type: "REQUEST",
        name: "createOrganization",
path: "createOrganization",
pathFormatted: "req_createorganizat-aeddb",
stats: {
    "name": "createOrganization",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "561",
        "ok": "-",
        "ko": "561"
    },
    "maxResponseTime": {
        "total": "561",
        "ok": "-",
        "ko": "561"
    },
    "meanResponseTime": {
        "total": "561",
        "ok": "-",
        "ko": "561"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "561",
        "ok": "-",
        "ko": "561"
    },
    "percentiles2": {
        "total": "561",
        "ok": "-",
        "ko": "561"
    },
    "percentiles3": {
        "total": "561",
        "ok": "-",
        "ko": "561"
    },
    "percentiles4": {
        "total": "561",
        "ok": "-",
        "ko": "561"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.062",
        "ok": "-",
        "ko": "0.062"
    }
}
    },"req_accountrequest-37419": {
        type: "REQUEST",
        name: "AccountRequest",
path: "AccountRequest",
pathFormatted: "req_accountrequest-37419",
stats: {
    "name": "AccountRequest",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "674",
        "ok": "-",
        "ko": "674"
    },
    "maxResponseTime": {
        "total": "674",
        "ok": "-",
        "ko": "674"
    },
    "meanResponseTime": {
        "total": "674",
        "ok": "-",
        "ko": "674"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "674",
        "ok": "-",
        "ko": "674"
    },
    "percentiles2": {
        "total": "674",
        "ok": "-",
        "ko": "674"
    },
    "percentiles3": {
        "total": "674",
        "ok": "-",
        "ko": "674"
    },
    "percentiles4": {
        "total": "674",
        "ok": "-",
        "ko": "674"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.062",
        "ok": "-",
        "ko": "0.062"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
